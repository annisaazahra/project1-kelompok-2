## ERD - "Aplikasi Undangan Nikahan Digital"

<p align="center"><a href="https://drive.google.com/drive/folders/1QlTFS7v6h2Trr_mNy9LKbNiARmo8K-fC?usp=sharing" target="_blank"><img src="https://gitlab.com/annisaazahra/project1-kelompok-2/-/raw/master/public/gambar/undangan_digital_v.2.png" width="400"></a>

</p>

## Tentang Project 1 Kelompok 2

Project 1 kami berjudul "Aplikasi Undangan Nikahan Digital". Harapannya pada aplikasi ini yaitu membuat suatu sistem aplikasi yang dapat memenuhi kebutuhan membuat undangan secara digital pada pasangan yang akan menikah. Namun, dikarenakan keterbatasan yang kami miliki dalam mengimplementasikan ilmu yang sudah didapatkan, maka inilah hasil akhir dari project 1 yang kami buat. 

## Informasi Kelompok 2 Grup 2
Berikut adalah nama kelompok 2 group 2 berserta username telegramnya :
1. Khaylannisa Ramadhani (nisaazzahrara)
2. Novi Fitri (nopifitri)
3. Dede Ilsan (Dede Ilsan)

## Penjelasan Singkat Mengenai Project yang Dikerjakan
1. Link Video Demo : https://gitlab.com/annisaazahra/project1-kelompok-2/-/blob/master/public/2022-03-13_23-16-12.mp4
2. Template yang kami gunakan yaitu AdminLTE. 
3. Menggunakan Laravel versi 6.
4. Link heroku : https://project1-grup2-kelompok2.herokuapp.com/register

Pada "Aplikasi Undangan Nikahan Digital" terdapat materi-materi yang telah dipelajari yaitu :

- Membuat ERD
- Migrations
- Model + Eloquent
- Controller
- Laravel Auth + Middleware
- View (Blade)
- CRUD (Create Read Update Delete)
- Eloquent Relationships
- Laravel + Library/Packages



