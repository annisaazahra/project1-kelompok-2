<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Produk;
use App\User;
use DB;
use RealRashid\SweetAlert\Facades\Alert;
use File;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        // $order = Order::all();
        $order = Order::where('users_id', auth()->user()->id)->get();
        $produk = Produk::all();
        $user = User::all();
        // return view('order.index', compact('order'));
        return view('order.index', compact('order', 'produk', 'user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $produk = DB::table('produk')->where('id', $id)->first();
        return view('order.create', compact('produk'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'mempelai_pria' => 'required',
            'mempelai_wanita' => 'required',
            'tanggal_acara' => 'required',
            'lokasi_acara' => 'required',
            'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ],
        [//untuk menganti pesan validasi
            'mempelai_pria.required' => 'Nama Harus diisi',
            'mempelai_wanita.required'  => 'Deskripsi tidak boleh kosong',
            'tanggal_acara.required'  => 'Tanggal Acara belum diisi',
            'lokasi_acara.required'  => 'Lokasi Acara belum diisi',
            'photo.required' => 'Photo prewed tidak boleh kosong'
        ]
        );
        $namaPhoto = time().'.'.$request->photo->extension();   //membuat nama file unik
   
        $request->photo->move(public_path('gambar'), $namaPhoto);

        $order = new Order;
        $order->mempelai_pria = $request->mempelai_pria;
        $order->mempelai_wanita = $request->mempelai_wanita;
        $order->tanggal_acara = $request->tanggal_acara;
        $order->lokasi_acara = $request->lokasi_acara;
        $order->photo = $namaPhoto;
        $order->users_id = auth()->user()->id;
        $order->produk_id = $request->produk_id;
        // dd($order);
        $order->save();
        Alert::success('Tambah', 'Order berhasil');
        return redirect('/order');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::findOrFail($id);
        return view('order.show', compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $produk = DB::table('produk')->get();
        $order = Order::findOrFail($id);
        return view('order.edit', compact('order'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'mempelai_pria' => 'required',
            'mempelai_wanita' => 'required',
            'tanggal_acara' => 'required',
            'lokasi_acara' => 'required',
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ],
        [//untuk menganti pesan validasi
            'mempelai_pria.required' => 'Nama mempelai pria tidak boleh kosong',
            'mempelai_wanita.required'  => 'Nama mempelai wanita tidak boleh kosong',
            'tanggal_acara.required'  => 'Tanggal Acara belum diisi',
            'lokasi_acara.required'  => 'Lokasi Acara belum diisi',
        ]
        );
        $order = Order::find($id);
        if($request->has('photo')){
        $namaPhoto = time().'.'.$request->photo->extension();   //membuat nama file unik
   
        $request->photo->move(public_path('gambar'), $namaPhoto);
        
            $order->mempelai_pria = $request->mempelai_pria;
            $order->mempelai_wanita = $request->mempelai_wanita;
            $order->tanggal_acara = $request->tanggal_acara;
            $order->lokasi_acara = $request->lokasi_acara;       
            $order->photo = $namaPhoto;
            // $order->produk_id = $request
        }else{
            $order->mempelai_pria = $request->mempelai_pria;
            $order->mempelai_wanita = $request->mempelai_wanita;
            $order->tanggal_acara = $request->tanggal_acara;
            $order->lokasi_acara = $request->lokasi_acara;
    
        }
        $order->save();
        Alert::success('Update', 'Data berhasil diupdate');
        return redirect('/order');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Order::find($id);
        
        $path = "gambar/";
        File::delete($path . $order->photo);
 
        $order->delete();
        Alert::success('Hapus', 'Order berhasil dibatalkan');
        return redirect('/order');
    }
}
