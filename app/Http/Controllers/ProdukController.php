<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produk;
use File;
use DB;
use RealRashid\SweetAlert\Facades\Alert;

class ProdukController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index', 'show');
 
    //     // $this->middleware('log')->only('index');
 
    //     // $this->middleware('subscribed')->except('store');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produk = Produk::all();
        return view('produk.index', compact('produk'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tema = DB::table('tema')->get();
        return view('produk.create', compact('tema'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    
        $request->validate([
            'nama_produk' => 'required|max:255',
            'deskripsi' => 'required',
            'tema_id' => 'required',
            'poster' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ],
        [//untuk menganti pesan validasi
            'nama_produk.required' => 'Nama Harus diisi',
            'nama_produk.max'  => 'Karakter tidak boleh lebih dari 255',
            'tema_id.required' => 'Tema Belum dipilih',
            'deskripsi.required'  => 'Deskripsi tidak boleh kosong',
            'poster.required' => 'Poster desain tidak boleh kosong'
        ]
        );
        $posterNama = time().'.'.$request->poster->extension();   //membuat nama file unik
   
        $request->poster->move(public_path('gambar'), $posterNama);
   
        $produk = new Produk;
        $produk->nama_produk = $request->nama_produk;
        $produk->deskripsi = $request->deskripsi;
        $produk->tema_id = $request->tema_id;
        $produk->poster = $posterNama;

        $produk->save();
        Alert::success('Tambah', 'Data berhasil ditambah');
        return redirect('/produk');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $produk = Produk::findOrFail($id);
        return view('produk.show', compact('produk'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tema = DB::table('tema')->get();
        $produk = Produk::findOrFail($id);
        return view('produk.edit', compact('produk', 'tema'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_produk' => 'required|max:255',
            'deskripsi' => 'required',
            'tema_id' => 'required',
            'poster' => '|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ],
        [
            'nama_produk.required' => 'Nama Harus diisi',
            'nama_produk.max'  => 'Karakter tidak boleh lebih dari 255',
            'tema_id.required' => 'Tema Belum dipilih',
            'deskripsi.required'  => 'Deskripsi tidak boleh kosong',
        ]
        );
        
        $produk = Produk::find($id);
        if($request->has('poster')){
        $posterNama = time().'.'.$request->poster->extension();   //membuat nama file unik
   
        $request->poster->move(public_path('gambar'), $posterNama);

            $produk->nama_produk = $request->nama_produk;
            $produk->deskripsi = $request->deskripsi;
            $produk->tema_id = $request->tema_id;
            $produk->poster = $posterNama;
        }else{
           
            $produk->nama_produk = $request->nama_produk;
            $produk->deskripsi = $request->deskripsi;
            $produk->tema_id = $request->tema_id;
        }
        $produk->save();
        Alert::success('Update', 'Data berhasil diupdate');
        return redirect('/produk');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produk = Produk::find($id);
        
        $path = "gambar/";
        File::delete($path . $produk->poster);
 
        $produk->delete();
        Alert::success('Hapus', 'Data berhasil dihapus');
        return redirect('/produk');
    }
}
