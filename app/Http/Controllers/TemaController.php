<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tema;

class TemaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }
    
    public function index()
    {
        $tema = Tema::all();

        return view('tema.index', compact('tema'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tema.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $request->validate([
                'nama_tema' => 'required'
            ],
            [
                'nama_tema.required' => 'Nama tema undangan harus diisi!'
            ]
        );
        
        $tema = new Tema;
        
        $tema->nama_tema = $request->nama_tema;
        
        $tema->save();

        return redirect('/tema');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tema = Tema::find($id);
        
        return view('tema.show', compact('tema'));
   }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tema = Tema::find($id);
        
        return view('tema.edit', compact('tema'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_tema' => 'required'
        ],
        [
            'nama_tema.required' => 'Nama tema undangan harus diisi!'
        ]
        );
        
        $tema = Tema::find($id);
        
        $tema->nama_tema = $request['nama_tema'];
        
        $tema->save();

        return redirect('/tema');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tema = Tema::find($id);
        $tema->delete();

        return redirect('/tema');
    }
}