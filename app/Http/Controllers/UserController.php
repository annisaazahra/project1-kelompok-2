<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        
        return view('pelanggan.index', compact('users'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = DB::table('users')->get();
        return view('pelanggan.create', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'password' =>'required' 
        ],
        [//untuk menganti pesan validasi
            'name.required' => 'Nama Harus diisi',
            'email.required'  => 'Email tidak boleh kosong',
            'password.required'  => 'Password tidak boleh kosong',
        ]);
        
        $users = new User;

        $users->name = $request->name;
        $users->email = $request->email;
        $users->password = $request->password;

        $users->save();
        
        return redirect('/user');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $users = User::findOrFail($id);
        
        return view('pelanggan.show', compact('users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $users = User::find($id);
        
        return view('pelanggan.edit', compact('users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'password' =>'required' 
        ],
        [//untuk menganti pesan validasi
            'name.required' => 'Nama Harus diisi',
            'email.required'  => 'Email tidak boleh kosong',
            'password.required'  => 'Password tidak boleh kosong',
        ]);
        
        $users = User::find($id);
        
        $users->name = $request['name'];
        $users->email = $request['email'];
        $users->password = $request['password'];
        
        $users->save();

        return redirect('/user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $users = User::find($id);
        $users->delete();

        return redirect('/user');
    }
}