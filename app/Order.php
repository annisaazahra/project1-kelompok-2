<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = "order";
    protected $fillable = ["produk_id", "users_id", "photo", "tanggal_acara", "mempelai_pria", "mempelai_wanita", "lokasi_acara"];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function produk()
    {
        return $this->belongsTo('App\Produk');
    }
}
