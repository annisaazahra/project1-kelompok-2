<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    protected $table = "produk";
    protected $fillable = ["nama_produk", "deskripsi", "tema_id", "poster"];

    public function tema()
    {
        return $this->belongsTo('App\Tema');
    }
    public function order()
    {
        return $this->hasMany('App\Order');
    }
}
