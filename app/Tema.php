<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tema extends Model
{
    protected $table = "tema";
    protected $fillable = ["nama_tema"];

    public function produk()
    {
        return $this->hasMany('App\Produk');
    }
}