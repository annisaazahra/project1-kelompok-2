@extends('layout.master')
@section('title')
Halaman Buat Order
@endsection

@section('content')
    <form method="post" action="/order" enctype="multipart/form-data">
        @csrf
       
        <div class="form-group">
            <label >Nama Mempelai Pria</label>
            <input type="text" name="mempelai_pria" class="form-control" >
        </div>
        {{-- untuk memunculkan alert jika validasi salah --}}
        @error('mempelai_pria') 
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label >Nama Mempelai Wanita</label>
            <input type="text" name="mempelai_wanita" class="form-control" >
        </div>
        {{-- untuk memunculkan alert jika validasi salah --}}
        @error('mempelai_wanita') 
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label >Tanggal Acara</label>
            <input type="date" name="tanggal_acara" class="form-control" >
        </div>
        {{-- untuk memunculkan alert jika validasi salah --}}
        @error('tanggal_acara') 
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Lokasi Acara</label>
            <textarea name="lokasi_acara" cols="30" rows="10" class="form-control"></textarea>        
        </div>
        @error('lokasi_acara')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Photo</label>
            <input type="file" name="photo" class="form-control">   
        </div>
        <input type="hidden" name="produk_id" value="{{$produk->id}}">
        <button type="submit" class="btn btn-primary">Submit</button>
        <a href="/produk" class="btn btn-secondary" style="float: right">Kembali</a>
    </form>
@endsection