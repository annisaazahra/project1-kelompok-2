@extends('layout.master')
@section('title')
Halaman Edit Order
@endsection

@section('content')
    <form method="post" action="/order/{{$order->id}}" enctype="multipart/form-data">
        @csrf
        @method('put')
        <div class="form-group">
            <label >Nama Mempelai Pria</label>
            <input type="text" value="{{$order->mempelai_pria}}" name="mempelai_pria" class="form-control" >
        </div>
        {{-- untuk memunculkan alert jika validasi salah --}}
        @error('mempelai_pria') 
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label >Nama Mempelai Wanita</label>
            <input type="text" value="{{$order->mempelai_wanita}}" name="mempelai_wanita" class="form-control" >
        </div>
        {{-- untuk memunculkan alert jika validasi salah --}}
        @error('mempelai_wanita') 
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label >Tanggal Acara</label>
            <input type="date" name="tanggal_acara" value="{{$order->tanggal_acara}}" class="form-control" >
        </div>
        {{-- untuk memunculkan alert jika validasi salah --}}
        @error('tanggal_acara') 
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Lokasi Acara</label>
            <textarea name="lokasi_acara" cols="30" rows="10" class="form-control">{{$order->lokasi_acara}}</textarea>        
        </div>
        @error('lokasi_acara')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Photo</label>
            <input type="file" name="photo" class="form-control">   
        </div>
    
        <input type="hidden" name="produk_id" value="{{$order->produk_id}}">
        {{-- <input type="hidden" name="users_id" value="{{$order->users_id}}"> --}}
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection