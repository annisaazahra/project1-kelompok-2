@extends('layout.master')
@section('title')
Halaman List Order
@endsection

@section('content')

<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama Pemesan</th>
        <th scope="col">Produk yang Dipesan</th>
        <th scope="col">Nama Produk</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($order as $key => $item)
        <tr>
            <td>{{$key+1}}</td>
        
            @foreach ($user as $keyUser => $itemUser)
                @if ($item->users_id == $itemUser->id)
                    <td>{{$itemUser->name}}</td>
                @endif
            @endforeach
            @foreach ($produk as $keyProduk => $itemProduk)
                @if ($item->produk_id == $itemProduk->id)
                <td><img src="{{asset('gambar/'.$itemProduk->poster)}}" alt="gambarDesainkartu" height="200px" width="200px"></td>
                <td>{{$itemProduk->nama_produk}}</td>
                @endif
            @endforeach

            <td>
                <form class="mt-2" action="/order/{{$item->id}}" method="POST">
                    @csrf
                    <a href="/order/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/order/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    @method('delete')
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                </form>
            </td>
        </tr>
        @empty
            <h1>Tidak Ada Produk yang diorder</h1>
        @endforelse
      {{-- @forelse ($order as $key => $item)
            @if ( Auth::user()->id  == $item->users_id)
            <tr>
                <td>{{$key+1}}</td>
                @foreach ($user as $keyUser => $itemUser)
                    @if ($item->users_id == $itemUser->id)
                        <td>{{$itemUser->name}}</td>
                    @endif
                @endforeach
                @foreach ($produk as $keyProduk => $itemProduk)
                @if ($item->produk_id == $itemProduk->id)
                    <td>{{$itemProduk->nama_produk}}</td>
                   
                @endif
               @endforeach 
               <td>
                    <form class="mt-2" action="/order/{{$item->id}}" method="POST">
                        @csrf
                        <a href="/order/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/order/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                        @method('delete')
                        <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                    </form>
                </td>
            </tr>
            @endif     
      @empty
          <h1>Tidak Ada Produk yang diorder</h1>
      @endforelse --}}
    </tbody>
  </table>
@endsection