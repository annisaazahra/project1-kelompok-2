@extends('layout.master')
@section('title')
Halaman Detail Order
@endsection

@section('content')

<label>Photo Prewed</label><br>
<img width="500" height="500" src="{{asset('gambar/'.$order->photo)}}" alt="gambar">

<table class="table mt-3" >
    <thead class="thead-dark">
      <tr>
        <th style="text-align: center" width="100">Keterangan</th>
        <th style="text-align: center" width="100">Isi</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><b>Nama Mempelai Pria</b></td>
        <td>{{$order->mempelai_pria}}</td>
      </tr>
      <tr>
        <td><b>Nama Mempelai Wanita</b></td>
        <td>{{$order->mempelai_wanita}}</td>
      </tr>
      <tr>
        <td><b>Tanggal Acara</b></td>
        <td>{{$order->tanggal_acara}}</td>
      </tr>
      <tr>
          <td><b>Lokasi Acara</b></td>
          <td>{{$order->lokasi_acara}}</td>
      </tr>
    </tbody>
  </table>
{{-- <button type="submit" class="btn btn-primary">Submit</button> --}}

<a href="/order" style="float:right" class="btn btn-secondary">Kembali</a>
@endsection