@extends('layout.master')
@section('title')
Halaman Edit Data Pelanggan
@endsection

@section('content')
<form method="POST" action="/user/{{$users->id}}">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Nama Pelanggan</label>
      <input type="text" name="name" value="{{$users->name}}" class="form-control">
    </div>

    @error('name')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <div class="form-group">
        <label>Email Pelanggan</label>
        <input type="email" name="email" value="{{$users->email}}" class="form-control">
      </div>
  
      @error('email')
          <div class="alert alert-danger">{{$message}}</div>
      @enderror

      <div class="form-group">
        <label>Password</label>
        <input type="password" name="password" value="{{$users->password}}" class="form-control">
      </div>
  
      @error('password')
          <div class="alert alert-danger">{{$message}}</div>
      @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>


@endsection