@extends('layout.master')
@section('title')
Halaman List Pelanggan
@endsection

@section('content')

<a href="/user/create" class="btn btn-primary mb-3">Tambah Pelanggan</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama Pelanggan</th>
        <th scope="col">Email</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($users as $key=> $item )
          <tr>
              <td>{{$key + 1}}</td>
              <td>{{$item->name}}</td>
              <td>{{$item->email}}</td>
              <td>
                  

                  <form class="mt-2" action="/user/{{$item->id}}" method="POST">
                    <a href="/user/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/user/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>

                    @csrf
                    @method('delete')
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                  </form>
              </td>
          </tr>
      @empty
          <h1>Data tidak ada!</h1>
      @endforelse
    </tbody>
  </table>


@endsection