@extends('layout.master')
@section('title')
Halaman Tambah Produk
@endsection

@push('style')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@push('script')
<script src="https://cdn.tiny.cloud/1/b0avh14jglw7nseu4u15vm3k0vcc2tb50y1qitqo973vlmfj/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
    });
  </script>

<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
    // In your Javascript (external .js resource or <script> tag)
$(document).ready(function() {
    $('.js-example-basic-single').select2();
});
</script>

@endpush

@section('content')
    <form method="post" action="/produk" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label >Nama Produk</label>
            <input type="text" name="nama_produk" class="form-control" >
        </div>
        {{-- untuk memunculkan alert jika validasi salah --}}
        @error('nama_produk') 
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Deskripsi</label>
            <textarea name="deskripsi" cols="30" rows="10" class="form-control"></textarea>        
        </div>
        @error('deskripsi')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror


        <div class="form-group">
            <label>Tema</label>
            <select name="tema_id" class="js-example-basic-single" style="width: 100%">
                <option value="">---Pilih Tema---</option>
                @foreach ($tema as $item)
                    <option value="{{$item->id}}">{{$item->nama_tema}}</option>
                @endforeach
            </select>    
        </div>
        @error('tema_id')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Poster</label>
            <input type="file" name="poster" class="form-control">   
        </div>
        <a href="/produk" class="btn btn-secondary" style="float: right">Kembali</a>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection