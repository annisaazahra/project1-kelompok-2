@extends('layout.master')
@section('title')
Halaman Edit Produk
@endsection

@push('script')
<script src="https://cdn.tiny.cloud/1/b0avh14jglw7nseu4u15vm3k0vcc2tb50y1qitqo973vlmfj/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
    });
  </script>

@endpush
@section('content')
    <form method="post" action="/produk/{{$produk->id}}" enctype="multipart/form-data">
        @csrf
        @method('put')
        <div class="form-group">
            <label >Nama Produk</label>
            <input type="text" value="{{$produk->nama_produk}}" name="nama_produk" class="form-control" >
        </div>
        {{-- untuk memunculkan alert jika validasi salah --}}
        @error('nama_produk') 
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Deskripsi</label>
            <textarea name="deskripsi" cols="30" rows="10" class="form-control">{{$produk->deskripsi}}</textarea>        
        </div>
        @error('deskripsi')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror


        <div class="form-group">
            <label>Tema</label>
            <select name="tema_id" class="form-control">
                <option value="">---Pilih Kategori---</option>
                @foreach ($tema as $item)
                    @if ($item->id === $produk->tema_id)
                        <option value="{{$item->id}}" selected>{{$item->nama_tema}}</option>
                        
                    @else
                    <option value="{{$item->id}}">{{$item->nama_tema}}</option>
                        
                @endif
                @endforeach
            </select>    
        </div>
        @error('tema_id')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror


        <div class="form-group">
            <label>Poster</label>
            <input type="file" name="poster" class="form-control">   
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection