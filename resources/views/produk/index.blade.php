@extends('layout.master')
@section('title')
Halaman List Produk
@endsection

@push('style')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush
@section('content')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
    // In your Javascript (external .js resource or <script> tag)
$(document).ready(function() {
    $('.js-example-basic-single').select2();
});
</script>
@push('script')
    
@endpush
@auth
<a href="/produk/create" class="btn btn-primary my-2">Tambah Produk</a>
@endauth


<div class="row">
    @forelse ($produk as $item)
        <div class="col-4">
            <div class="card" style="width: 18rem;">
                <img src="{{asset('gambar/'.$item->poster)}}" class="card-img-top" height="200px" alt="gambarDesain">
                <div class="card-body">
                <span class="badge badge-info">{{$item->tema->nama_tema}}</span>
                <h3>{{$item->nama_produk}}</h3>
                <p class="card-text">{{Str::limit($item->deskripsi, 30,)}}</p>
                @auth
                <form action="/produk/{{$item->id}}" method="POST">
                    @csrf
                    @method("DELETE")
                    <a href="/produk/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/produk/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                </form>
                @endauth
                @guest
                <a href="/produk/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                @endguest
                </div>
            </div>
        </div>
    
    @empty
        <h4>Data Produk Belum Ada</h4>
    @endforelse
</div>

@endsection