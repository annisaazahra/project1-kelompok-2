@extends('layout.master')
@section('title')
Halaman Detail Produk {{$produk->nama_produk}}
@endsection

@section('content')

<img src="{{asset('gambar/'.$produk->poster)}}" alt="logoposter">
<h1>{{$produk->nama_produk}}</h1>
<p>{{$produk->deskripsi}}</p>


<a href="/order/create/{{$produk->id}}" class="btn btn-primary" >Order</a>
<a href="/produk" class="btn btn-secondary" style="float: right">Kembali</a>

{{-- <form action="/order/create/{{$produk->id}}" method="POST">
    @csrf
    @method('get')
    <input type="hidden" name="produk_id" value="{{$produk->id}}">
    <button type="submit" class="btn btn-primary">Order</button>
</form> --}}
@endsection