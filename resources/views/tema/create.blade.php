@extends('layout.master')
@section('title')
Halaman Tambah Tema
@endsection

@push('style')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@push('script')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<script>
// In your Javascript (external .js resource or <script> tag)
  $(document).ready(function() {
    $('.js-example-basic-single').select2();
  });
</script>
@endpush
  

@section('content')
<form method="POST" action="/tema">
    @csrf
    <div class="form-group">
      <label>Nama Tema Undangan</label>
      <input type="text" name="nama_tema" class="form-control">
    </div>

    @error('nama_tema')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>


@endsection