@extends('layout.master')
@section('title')
Halaman Edit Tema
@endsection

@section('content')
<form method="POST" action="/tema/{{$tema->id}}">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Nama Tema Undangan</label>
      <input type="text" name="nama_tema" value="{{$tema->nama_tema}}" class="form-control">
    </div>

    @error('nama_tema')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>


@endsection