@extends('layout.master')
@section('title')
Halaman List Tema Undangan
@endsection

@section('content')

@auth
<a href="/tema/create" class="btn btn-primary mb-3">Tambah Tema</a>
@endauth


<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama Tema</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($tema as $key=> $item )
          <tr>
              <td>{{$key + 1}}</td>
              <td>{{$item->nama_tema}}</td>
              <td>
                  
                @auth
                <form class="mt-2" action="/tema/{{$item->id}}" method="POST">
                  <a href="/tema/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                  
                  <a href="tema/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>

                  @csrf
                  @method('delete')
                  <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                  
                </form>
                @endauth
                @guest
                <a href="/tema/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                @endguest
              </td>
          </tr>
      @empty
          <h1>Data tidak ada!</h1>
      @endforelse
    </tbody>
  </table>


@endsection