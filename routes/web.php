<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('/', 'DashboardController@index');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', 'DashboardController@index');


Route::group(['middleware' => ['auth']], function () {

//CRUD Order
Route::resource('/order', 'OrderController');

//CRUD Pelanggan/User
Route::resource('/user', 'UserController');

//CRUD Produk
Route::resource('/produk', 'ProdukController');

//CRUD Order
 //read, update, delete
Route::resource('/order', 'OrderController')->only(['index', 'update', 'edit', 'store', 'destroy', 'show']);

//create
Route::get('/order/create/{produk_id}', 'OrderController@create');
Route::post('/order', 'OrderController@store');
});

//CRUD Tema
//create
Route::get('/tema/create', 'TemaController@create');
Route::post('/tema', 'TemaController@store');

//Read
Route::get('/tema', 'TemaController@index');
Route::get('/tema/{tema_id}', 'TemaController@show');

//Update
Route::get('/tema/{tema_id}/edit', 'TemaController@edit');
Route::put('/tema/{tema_id}', 'TemaController@update');

//Delete
Route::delete('/tema/{tema_id}', 'TemaController@destroy');

//CRUD Produk
Route::resource('/produk', 'ProdukController');

Auth::routes();

?>